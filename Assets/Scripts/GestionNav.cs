using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GestionNav : MonoBehaviour
{
    [SerializeField] private Transform PlayerTransform;
    [SerializeField] private GameObject NAV;
    [SerializeField] private Animator NAVAnim;
    private RandomAudioManager randomAudioManager = null;

    [SerializeField] private Transform RobotPlaceHolder;
    private bool moving = false;


    private void Update()
    {
        if (moving)
        {
            NAV.GetComponent<NavMeshAgent>().stoppingDistance = 2.0f;
            if (NAV.GetComponent<NavMeshAgent>().enabled == true && NAV.GetComponent<NavMeshAgent>().remainingDistance - NAV.GetComponent<NavMeshAgent>().stoppingDistance > 0.5f)
            {
                NAVAnim.SetBool("Walk_Anim", true);
                
            }
            else
            {
                NAVAnim.SetBool("Walk_Anim", false);
                NAV.GetComponent<NavMeshAgent>().enabled = false;
                //NAV.GetComponent<RobotBouleProche>().enabled = true;
                moving = false;
                if (randomAudioManager != null && randomAudioManager.GetPlayerState() == 2)
                {
                    NAV.transform.position = new Vector3(0,0,0);
                }
            }
        }
    }

    public void Notify(int i)
    {
        switch (i)
        {
            // Actions r�alis�es par le player
            case (0):
                StartCoroutine(SpawnNav());
                break;
            case (1):
                StartCoroutine(HubCompleted());
                break;
            case (2):
                StartCoroutine(EntreeEnigme1());
                break;
            case (3):
                StartCoroutine(Enigme1Completed());
                break;
            case (4):
                StartCoroutine(EntreeSalleFin());
                break;
        }
    }

    IEnumerator SpawnNav()
    {
        //Spawn de NAV
        yield return new WaitForSeconds(0f);
        NAV.SetActive(true);
        NAVAnim.SetBool("Open_Anim", false);

        //NAV entre dans la pi�ce
        yield return new WaitForSeconds(9.0f);
        NAV.GetComponent<AudioSourceNAV>().playAudio(0);

        // Nav sort de sa boule
        yield return new WaitForSeconds(5.0f);
        NAV.transform.rotation = Quaternion.identity;
        NAVAnim.SetBool("Open_Anim", true);

        // Nav se d�place vers vous
        yield return new WaitForSeconds(5.0f);
        DeplacementNav();
        NAV.GetComponent<NavMeshAgent>().SetDestination(PlayerTransform.position);

        // Nav Parle une deuxi�me fois
        NAV.GetComponent<AudioSourceNAV>().playAudio(1);
    }

    IEnumerator HubCompleted()
    {
        // Player a ouvert la salle
        randomAudioManager = GameObject.Find("RandomAudio").GetComponent<RandomAudioManager>();
        randomAudioManager.SetPlayerState(-1);

        // Play Success audio
        yield return new WaitForSeconds(1f);
        NAV.GetComponent<AudioSourceNAV>().playAudio(2);

        // Se d�place � la deuxi�me salle
        yield return new WaitForSeconds(2f);
        DeplacementNav();

        // /!\ Destination � modifier !
        NAV.GetComponent<NavMeshAgent>().SetDestination(PlayerTransform.position);
    }

    IEnumerator EntreeEnigme1()
    {
        // Player dans l'�nigme 1
        randomAudioManager = GameObject.Find("RandomAudio").GetComponent<RandomAudioManager>();
        randomAudioManager.SetPlayerState(1);

        // Play Success audio
        yield return new WaitForSeconds(1f);
        NAV.GetComponent<AudioSourceNAV>().playAudio(3);

        // Se d�place � la deuxi�me salle
        yield return new WaitForSeconds(2f);
        DeplacementNav();

        // Se d�place vers le mur
        NAV.GetComponent<NavMeshAgent>().SetDestination(new Vector3(4.6f, 0f, 31.5f));

        //TP NAV � la salle finale
        yield return new WaitForSeconds(5f);
        NAV.GetComponent<Rigidbody>().isKinematic = true;
        NAV.GetComponent<SphereCollider>().enabled = false;
        NAV.GetComponent<NavMeshAgent>().enabled = false;
        NAV.GetComponent<RobotBouleProche>().enabled = true;

        NAVAnim.SetBool("Walk_Anim", false);
        NAV.transform.SetParent(RobotPlaceHolder);
        yield return new WaitForSeconds(1f);
        NAV.transform.position = RobotPlaceHolder.position;
        NAV.transform.rotation = Quaternion.identity;


    }

    IEnumerator Enigme1Completed()
    {
        randomAudioManager.SetPlayerState(1);
        yield return new WaitForSeconds(1f);

        // Nav dit au joueur qu'il doit aller � la salle suivante
        NAV.GetComponent<AudioSourceNAV>().playAudio(4);
    }

    IEnumerator EntreeSalleFin()
    {
        randomAudioManager.SetPlayerState(2);
        yield return new WaitForSeconds(2f);
        NAV.GetComponent<AudioSourceNAV>().playAudio(5);

        yield return new WaitForSeconds(5f);
        
    }

    private void DeplacementNav()
    {
        //NAV.GetComponent<RobotBouleProche>().enabled = false;
        NAV.GetComponent<NavMeshAgent>().enabled = true;
        moving = true;
    }
}
