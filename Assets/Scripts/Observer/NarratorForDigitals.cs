using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NarratorForDigitals : MonoBehaviour, IObserver
{
    [SerializeField] Subject _playerSubject;
    [SerializeField] AudioClip _stop; //ne pas oublier d'ajouter component audiosource dans le gameobject


    private void Awake()
    {
    }

    // Permet de d�finir les actions � r�aliser en fonction des actions observ�es
    public void OnNotify(CustomActions action)
    {
        switch (action)
        {
            case (CustomActions.PlacedOnTheGoodPlace):
                // on allume les lumieres

                break;

            case (CustomActions.PLacedOnTheGoodNumber):

                break;
        }
    }


    private void OnEnable()
    {
        _playerSubject.AddObserver(this);
    }

    private void OnDisable()
    {
        _playerSubject.RemoveObserver(this);
    }
}
