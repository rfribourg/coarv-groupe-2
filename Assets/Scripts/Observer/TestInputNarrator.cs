using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestInputNarrator : Subject
{
    public float speed = 10f; //Controls velocity multiplier


    void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            Debug.Log("saut");
            NotifyObservers(CustomActions.Jump);
            transform.Translate(new Vector3(0, 1, 0)*speed*Time.deltaTime);
        }
    }
}
