using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Subject : MonoBehaviour
{
    //collection d'observers pour ce subjsct
    private List<IObserver> _observers = new List<IObserver>();

    //Ajoute observer � la liste
    public void AddObserver(IObserver observer)
    {
        _observers.Add(observer);
    }

    //enl�ve observer � la liste
    public void RemoveObserver(IObserver observer)
    {
        _observers.Remove(observer);
    }

    //informe Tous les observers d'une action 
    protected void NotifyObservers(CustomActions action)
    {
        _observers.ForEach((_observers) =>
        {
            _observers.OnNotify(action);
        });
    }
}
