using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NarratorSystem : MonoBehaviour, IObserver
{
    [SerializeField] Subject _playerSubject;
    [SerializeField] AudioClip _stop; //ne pas oublier d'ajouter component audiosource dans le gameobject
    AudioSource _audioPlayer;


    private void Awake()
    {
        _audioPlayer = GetComponent<AudioSource>();
    }

    // Permet de d�finir les actions � r�aliser en fonction des actions observ�es
    public void OnNotify(CustomActions action)
    {
        switch (action)
        {
            // Actions r�alis�es par le player
            case (CustomActions.Jump):
                _audioPlayer.clip = _stop;
                _audioPlayer.Play();
                break;
        }
    }

    
    private void OnEnable()
    {
        _playerSubject.AddObserver(this);
    }

    private void OnDisable()
    {
        _playerSubject.RemoveObserver(this);
    }
}
