using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SpawnNAV : MonoBehaviour, IObserver
{
    [SerializeField] Subject _playerSubject;
    [SerializeField] GameObject NAV;
    [SerializeField] AudioSource AudioNAVSpawn;
    [SerializeField] Animator NAVAnim;

/*    private void Update()
    {
        if (NAV.GetComponent<NavMeshAgent>().remainingDistance - NAV.GetComponent<NavMeshAgent>().stoppingDistance > 0)
        {
            NAVAnim.SetBool("Walk_Anim", true);
        }
        else
        {
            NAVAnim.SetBool("Walk_Anim", false);
        }
    }*/

    // Permet de d�finir les actions � r�aliser en fonction des actions observ�es
    public void OnNotify(CustomActions action)
    {
        switch (action)
        {
            // Actions r�alis�es par le player
            case (CustomActions.Spawn):
                StartCoroutine(SpawnNav());
                break;
        }
    }

    IEnumerator SpawnNav()
    {
        //Spawn de NAV
        yield return new WaitForSeconds(2f);
        NAV.SetActive(true);
        NAVAnim.SetBool("Open_Anim", false);
        //NAV entre dans la pi�ce
        yield return new WaitForSeconds(9.0f);
        NAV.GetComponent<AudioSourceNAV>().playAudio(0);

        // Nav se d�place vers vous
        yield return new WaitForSeconds(15.0f);
        NAV.GetComponent<NavMeshAgent>().enabled = true;
        NAVAnim.SetBool("Open_Anim", true);
        yield return new WaitForSeconds(3.0f);
        Vector3 destination = new Vector3(2, NAV.transform.position.y, 2);
        NAV.GetComponent<NavMeshAgent>().SetDestination(destination);
    }

    private void OnEnable()
    {
        _playerSubject.AddObserver(this);
    }

    private void OnDisable()
    {
        _playerSubject.RemoveObserver(this);
    }
}
