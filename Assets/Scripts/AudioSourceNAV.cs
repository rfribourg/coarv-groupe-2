using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioSourceNAV : MonoBehaviour
{
    public List<AudioSource> listeAudio;

    public void playAudio(int i)
    {
        for (int j = 0; j < listeAudio.Count; j++)
        {
            if (j != i)
            {
                listeAudio[j].Stop();
            }
        }
        listeAudio[i].Play();
    }


}
