using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RobotBouleProche : MonoBehaviour
{
    [SerializeField] private Transform leftController;
    [SerializeField] private Transform rightController;
    [SerializeField] private Transform robotTransform;
    [SerializeField] private Animator anim;

    private float distanceLeft = 0.0f;
    private float distanceRight = 0.0f;
    private bool grabMode;

    private void Start()
    {
        grabMode = false;
        anim.SetBool("Walk_Anim", false);
    }
    private void Update()
    {
        distanceLeft = Vector3.Magnitude(leftController.position - robotTransform.position);
        distanceRight = Vector3.Magnitude(rightController.position - robotTransform.position);
        if (!grabMode)
        {
            grabMode = verifGrab(grabMode, distanceLeft, distanceRight);
        }
        
        
    }

    private void bouleMode()
    {
        anim.SetBool("Open_Anim", false);
        GetComponent<NavMeshAgent>().enabled = false;
    }

    private bool verifGrab(bool _grabMode, float _distanceLeft, float _distanceRight)
    {
        if (_distanceLeft < 1.0f || _distanceRight < 1.0f)
        {
            GetComponent<AudioSourceNAV>().playAudio(6);
            bouleMode();
            return true;
        }
        return false;
    }
}
