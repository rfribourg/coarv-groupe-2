using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlueCardValidation : Subject
{
    public GameObject GlassTank;
    public AudioSource SuccessAudio;
    public AudioSource ErrorAudio;
    [SerializeField] GameObject RobotWD;
    [SerializeField] GameObject GoldenCard;
    private GestionNav Nav;

    private void Start()
    {
        if (GameObject.Find("GestionNav") != null)
        {
            Nav = GameObject.Find("GestionNav").GetComponent<GestionNav>();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        // Si c'est la bonne carte qui est pos�e, on ouvre le GlassTank
        if (other.name == "BlueCard")
        {
            Debug.LogWarning("in");
            GlassTank.SetActive(false);
            SuccessAudio.Play();
            GameObject.Destroy(GameObject.Find("BlueCard"));
            StartCoroutine(RobotWDSetFalse());
            if(Nav != null)
            {
                Nav.Notify(0);
            }
            else if (GameObject.Find("GestionNav") != null)
            {
                Nav = GameObject.Find("GestionNav").GetComponent<GestionNav>();
                Nav.Notify(0);
            }
        }
    }

    IEnumerator RobotWDSetFalse()
    {
        RobotWD.GetComponentInChildren<DissolutionScript>().DissolutionFunc();
        RobotWD.GetComponent<WDDialogue>().StopSound();
        yield return new WaitForSeconds(5);
        RobotWD.SetActive(false);
        GoldenCard.SetActive(true);
    }

}
