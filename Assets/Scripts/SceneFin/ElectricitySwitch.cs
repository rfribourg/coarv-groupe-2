using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectricitySwitch : MonoBehaviour
{
    [SerializeField]
    GameObject filledSocleElec;
    [SerializeField]
    GameObject emptySocleElec;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetToFilledSocleElec()
    {
        filledSocleElec.SetActive(true);
        emptySocleElec.SetActive(false);
    }

    public void SetToEmptySocleElec()
    {
        filledSocleElec.SetActive(false);
        emptySocleElec.SetActive(true);
    }
}
