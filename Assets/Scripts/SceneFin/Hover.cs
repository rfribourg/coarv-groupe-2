using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hover : MonoBehaviour
{
    public float amplitude = 1;
    public float frequency = 1;


    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Turn());
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = transform.position;
        pos.y = amplitude * Mathf.Sin(frequency * 2 * Mathf.PI * Time.time);
        transform.position = pos;
    }

    IEnumerator Turn()
    {
        yield return new WaitForSeconds(5);

        Vector3 rot = new Vector3(0, 90, 0);
        Vector3 startRot = transform.localEulerAngles;
        for(float alpha = 0; alpha <= 1; alpha+=0.01f)
        {
            transform.localEulerAngles = Vector3.Lerp(startRot, rot, alpha);
            yield return new WaitForSeconds(0.01f);
        }
        transform.localEulerAngles = rot;
    }
}
