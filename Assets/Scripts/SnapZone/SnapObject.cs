using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnapObject : MonoBehaviour
{
    public GameObject SnapLocation;
    public bool isSnapped;
    private bool objectSnapped;
    private bool grabbed;

    private void Update()
    {
        grabbed = GetComponent<OVRGrabbable>().isGrabbed;

        objectSnapped = SnapLocation.GetComponent<SnapToPosition>().Snapped;

        if (objectSnapped == true)
        {
            GetComponent<Rigidbody>().isKinematic = true;
            transform.SetParent(SnapLocation.transform);
            isSnapped = true;
        }

        if (objectSnapped == false && grabbed == false)
        {
            GetComponent<Rigidbody>().isKinematic = false;
        }
    }
}
