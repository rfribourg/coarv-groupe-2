using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnapZone : MonoBehaviour
{
    public Transform ZoneTransform;
    public GameObject TableauPremiereEtape;
    public GameObject TableauDeuxiemeEtape;
    public GameObject EventManagerEnigme1;

    //True if the object colliding with the zone is grabbed
    private bool grabbed = false;
    public bool BonTableau;

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer == 8)
        {
            SnapObject(other.gameObject);
            grabbed = other.GetComponent<OVRGrabbable>().isGrabbed;
        }
    }

    private void SnapObject(GameObject other)
    {
        //Si on lache l'objet alors qu'il est dans la zone, il se snap
        if (!grabbed)
        {
            other.transform.SetPositionAndRotation(ZoneTransform.position, ZoneTransform.rotation);
            Debug.LogWarning("kinematic");

            if (!EventManagerEnigme1.GetComponent<Enigme1FirstPart>().PremiereEtapeFinie)
            {
                BonTableau = other.gameObject == TableauPremiereEtape;
            }
            else
            {
                BonTableau = other.gameObject == TableauDeuxiemeEtape;
            }
        }
    }
}
