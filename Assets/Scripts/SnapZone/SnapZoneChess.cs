using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEditor.Rendering;
using UnityEngine;
using UnityEngine.ProBuilder.Shapes;

public class SnapZoneChess : MonoBehaviour
{
    public Transform ZoneTransform;
    public GameObject Cube;
    public GameObject Tableau;

    private float temps;
    public bool Snapped;
    //True if the object colliding with the zone is grabbed
    private bool grabbed = true;
    private bool moving;

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == 10)
        {
            other.GetComponent<Rigidbody>().isKinematic = false;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer == 10)
        {
            grabbed = other.GetComponent<OVRGrabbable>().isGrabbed;
            Debug.Log("is grabbed" + grabbed);
            SnapObject(other.gameObject);
        }
    }

    private void SnapObject(GameObject other)
    {
        //Si on lache l'objet alors qu'il est dans la zone, il se snap
        if (!grabbed)
        {
            other.transform.SetPositionAndRotation(ZoneTransform.position, ZoneTransform.rotation);
            other.GetComponent<Rigidbody>().isKinematic = true;
            Snapped = true;
            if (!moving)
            {
                temps = 0.0f;
                moving = true;
                StartCoroutine(MoveCube());
            }
        }
    }

    IEnumerator MoveCube()
    {
        GetComponent<AudioSource>().Play();
        while (temps < 1)
        {
            temps += Time.deltaTime;
            // Move the door
            Cube.transform.Translate(-Vector3.forward * Time.deltaTime * 0.1f, Space.Self);
            yield return new WaitForEndOfFrame();
        }
        Tableau.SetActive(true);
    }
}
