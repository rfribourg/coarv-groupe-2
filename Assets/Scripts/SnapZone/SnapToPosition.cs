using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnapToPosition : MonoBehaviour
{
    private bool grabbed;
    private bool insideSnapZone;
    public bool Snapped;

    public GameObject Tableau;
    public GameObject SnapRotationReference;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == Tableau.name)
        {
            insideSnapZone = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == Tableau.name)
        {
            insideSnapZone = false;
        }
    }

    private void SnapObject()
    {
        if (!grabbed && insideSnapZone)
        {
            Tableau.gameObject.transform.position = transform.position;
            Tableau.gameObject.transform.rotation = SnapRotationReference.transform.rotation;
            Snapped = true;
        }
    }

    private void UnsnapObject()
    {
        if (grabbed)
        {
            Snapped = false;
        }
    }
    private void Update()
    {
        grabbed = Tableau.GetComponent<OVRGrabbable>().isGrabbed;
        SnapObject();
    }
}
