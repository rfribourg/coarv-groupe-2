using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldenCardValidation : Subject
{
    public GameObject Door;
    public AudioSource SuccessAudio;
    public AudioSource ErrorAudio;
    private GestionNav Nav;
    public GameObject HallwayObstacle;                      // This object prevents the user from TPing into the hallway before the door is open

    private float temps;
    private void Start()
    {
        
        if (GameObject.Find("GestionNav") != null)
        {
            Nav = GameObject.Find("GestionNav").GetComponent<GestionNav>();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "GoldenCard")
        {
            SuccessAudio.Play();

            // Coroutine to open the door
            temps = 0.0f;
            StartCoroutine(OpenDoor());
            if (Nav != null)
            {
                Nav.Notify(1);
            }
            
        }
        else if (other.name == "BlueCard")
        {
            ErrorAudio.Play();
        }
    }

    IEnumerator OpenDoor()
    {
        while (temps < 2)
        {
            temps += Time.deltaTime;
            // Move the door
            Door.transform.Translate(-Vector3.forward * Time.deltaTime, Space.Self);
            yield return new WaitForEndOfFrame();
        }
        HallwayObstacle.SetActive(false);
    }
}
