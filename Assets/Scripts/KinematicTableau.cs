using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KinematicTableau : MonoBehaviour
{
    public bool IsSnapped;
    void Update()
    {
        if (!IsSnapped && !GetComponent<OVRGrabbable>().isGrabbed && GetComponent<Rigidbody>().isKinematic)
        {
            GetComponent<Rigidbody>().isKinematic = false;
        }
    }
}
