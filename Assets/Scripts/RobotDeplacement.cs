using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RobotDeplacement : MonoBehaviour
{
    [SerializeField] NavMeshAgent agent;
    [SerializeField] GameObject robot;
    [SerializeField] Transform playerTarget;
    // Animation
    Animator anim;

    private void Start()
    {
        agent.updateRotation = true;
        anim = robot.GetComponent<Animator>();
        agent.stoppingDistance = 3;
    }
    void Update()
    {
        agent.SetDestination(playerTarget.position);

        if (agent.remainingDistance - agent.stoppingDistance > 0)
        {
            anim.SetBool("Walk_Anim", true);
        }
        else
        {
            anim.SetBool("Walk_Anim", false);
        }
    }


}
