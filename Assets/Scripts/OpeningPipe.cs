using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpeningPipe : MonoBehaviour
{
    [SerializeField] Transform PiovtAnchor;
    private bool pipeOpen;
    private void OnTriggerEnter(Collider other)
    {
        if(!pipeOpen && other.gameObject.name == "NAV")
        {
            pipeOpen = true;
            transform.RotateAround(PiovtAnchor.position, Vector3.forward, -90);
        }
    }
}
