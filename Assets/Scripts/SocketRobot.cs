using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class SocketRobot : MonoBehaviour
{
    // This script is used to activate the end
    // When NAV is placed in the socket, it activates the phone
    public bool NavSacrifie;
    [SerializeField]
    GameObject robot;
    [SerializeField]
    GameObject elecHandler;
    ElectricitySwitch elecSwitch;

    // Start is called before the first frame update
    void Start()
    {
        elecSwitch = elecHandler.GetComponent<ElectricitySwitch>();
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == robot)
        {
            //Destroy(door);
            gameObject.GetComponent<MeshRenderer>().enabled = false;
            robot.GetComponent<Rigidbody>().isKinematic = true;
            robot.GetComponent<Rigidbody>().useGravity = false;
            robot.GetComponent<SphereCollider>().isTrigger = true;
            StartCoroutine(MoveRobotToCenter());
        }
    }

    IEnumerator MoveRobotToCenter()
    {
        Transform robotTrans = robot.transform;
        Vector3 startPos = robotTrans.position;
        Vector3 endPos = transform.position;
        
        
        for (float alpha = 0; alpha <= 1; alpha+=0.01f)
        {
            robot.transform.position = Vector3.Lerp(startPos, endPos, alpha);
            yield return new WaitForSeconds(0.01f);
        }
        elecSwitch.SetToFilledSocleElec();
        NavSacrifie = true;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(3f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }
}
