using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine;

public class StartSceneScreens : MonoBehaviour
{
    public AudioSource ComputerNoises;
    public List<GameObject> _textsMP;

    [SerializeField] private GameObject centerEyeAnchor;

    private int numberOfPokes;
    private TMP_Text _petitspointsText;
    private bool onCoroutine;
    void Start()
    {
        numberOfPokes = 0;
        for (int i = 1; i < _textsMP.Count; i++)
        {
            _textsMP[i].SetActive(false);
        }
    }

    public void RedButtonPoked()
    {
        // the button does smg only if we're out of the coroutine
        if (!onCoroutine) {

            // we increase the number of pokes
            numberOfPokes += 1;
            // and the volume of the pc
            ComputerNoises.volume += 0.1f;

            if (numberOfPokes < _textsMP.Count) {

                // and change the text
                _textsMP[numberOfPokes - 1].SetActive(false);
                _textsMP[numberOfPokes].SetActive(true);

                // coroutine for the three little dots
                if (numberOfPokes == 2)
                {
                    StartCoroutine(TroisPetitsPoints());
                }

                // once the player has pressed enough times, we load the scene
                // - TODO - Animation du player qui rentre dans l'ecran ?
                if (numberOfPokes == _textsMP.Count - 1)
                {
                    StartCoroutine(Transition());
                }
            }
        }
    }

    IEnumerator TroisPetitsPoints()
    {
        onCoroutine = true;
        for (int i = 0; i<3; i++)
        {
            yield return new WaitForSeconds(0.6f);
            _textsMP[2].GetComponent<TextMeshProUGUI>().text += ".";
        }

        yield return new WaitForSeconds(1f);

        _textsMP[2].GetComponent<TextMeshProUGUI>().text = "No seriously stop bugging me";

        onCoroutine = false;
    }

    IEnumerator Transition()
    {
        onCoroutine = true;

        yield return new WaitForSeconds(2.0f);
        Debug.Log("Start fadeout");
        centerEyeAnchor.GetComponent<OVRScreenFade>().FadeOut();
        yield return new WaitForSeconds(3.0f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        onCoroutine = false;
    }
}
