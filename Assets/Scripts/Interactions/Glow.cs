using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Glow : MonoBehaviour
{
    // This script makes an object glow 
    // The color is selectable from the editor (outlineColor)
    // The outline starts to appear when the user's closest hand is at glowStartDistance
    // The outline appears at full strength when the user's closest hand is at glowFullDistance

    [SerializeField]
    private float glowStartDistance = 3f;
    [SerializeField]
    private float glowFullDistance = 0.5f;
    private float d, d0, d1;
    GameObject[] hands;

    [SerializeField]
    private Color outlineColor = new Color(1.0f, 0.86f, 0.0f);
    private Color displayColor;

    Outline outlineScript;

    void Start()
    {
        d0 = 10000;
        d1 = 10000;
        hands = GameObject.FindGameObjectsWithTag("HandAnchor");        // Uesr's HandAnchors must have the "HandAnchor" tag to be detected

        //Debug.Log("Number of HandAnchors : " + hands.Length);

        //outlineColor = new Color(1.0f, 0.86f, 0.0f);

        outlineScript = gameObject.GetComponent<Outline>();
        displayColor = outlineColor;
    }

    // Update is called once per frame
    void Update()
    {
        if(hands.Length > 1)
        {
            d0 = Vector3.Distance(transform.position, hands[0].transform.position);
            d1 = Vector3.Distance(transform.position, hands[1].transform.position);

            d = Mathf.Min(d0, d1);
        }
        else if(hands.Length > 0)
        {
            d = Vector3.Distance(transform.position, hands[0].transform.position);
        }
        else
        {
            return;
        }

        if (d < glowFullDistance)
        {
            //Debug.Log("GLOW : Full color");
            displayColor = outlineColor;
        }
        else if (d < glowStartDistance)
        {
            //Debug.Log("GLOW : Mid color");
            displayColor = outlineColor;
            displayColor.a = (glowStartDistance - d) / (glowStartDistance - glowFullDistance);
        }
        else
        {
            //Debug.Log("GLOW : No color");
            displayColor = outlineColor;
            displayColor.a = 0;
        }
        outlineScript.OutlineColor = displayColor;
    }
}



