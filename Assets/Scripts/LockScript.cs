using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockScript : MonoBehaviour
{
    public GameObject RedButton;
    public GameObject Glass;
    public float KeyRotationSpeed;
    public float GlassRotationSpeed;

    private Vector3 keyPositionInLock;
    private Transform GlassPivot;

    // Script attached to a lock to interact with a key
    void Start()
    {
        keyPositionInLock = Vector3.zero;
        keyPositionInLock.y = 0.45f;
        GlassPivot = Glass.transform.GetChild(0);
    }

    private void OnTriggerEnter(Collider other)
    {
        // we check if the collider is a key
        if (other.gameObject.layer == 7)
        {           
            // the key becomes kinematic (prevents from falling through the lock)
            other.GetComponent<Rigidbody>().isKinematic = true;

            // we set the lock as parent of the key
            other.gameObject.transform.parent = this.gameObject.transform;

            // set the position and rotation of the key in the lock
            other.transform.SetLocalPositionAndRotation(keyPositionInLock, Quaternion.Euler(Vector3.zero));

            // Disable the scripts so that we can't interact with the key anymore
            other.GetComponent<Oculus.Interaction.GrabInteractable>().enabled = false;
            other.GetComponent<Oculus.Interaction.PhysicsGrabbable>().enabled = false;
            other.GetComponent<Oculus.Interaction.Grabbable>().enabled = false;

            // Coroutine for the rotation animation 
            StartCoroutine(KeyRotation(other));
        }
    }


    IEnumerator KeyRotation(Collider other)
    {
        yield return new WaitForSeconds(0.3f);
        while (other.transform.localEulerAngles.y < 90)
        {
            //rotate at KeyRotationSpeed degrees per second
            other.transform.Rotate(Vector3.up * KeyRotationSpeed * Time.deltaTime);

            //wait for next frame
            yield return new WaitForEndOfFrame();
        }

        //When the key has finished to rotate, we open the protection glass
        StartCoroutine(GlassRotation());
    }

    IEnumerator GlassRotation()
    {
        yield return new WaitForSeconds(0.2f);
        while (Glass.transform.localEulerAngles.y < 90)
        {
            //rotate at glassRotationSpeed degrees per second around its pivot
            Glass.transform.RotateAround(GlassPivot.transform.position, Vector3.right, GlassRotationSpeed * Time.deltaTime);

            //wait for next frame
            yield return new WaitForEndOfFrame();
        }

        // when the protection Glass is open, the button can be activated
        RedButton.GetComponent<Oculus.Interaction.PokeInteractable>().enabled = true;
        RedButton.GetComponent<Oculus.Interaction.InteractableUnityEventWrapper>().enabled = true;
    }

}
