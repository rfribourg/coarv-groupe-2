using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WDDialogue : MonoBehaviour
{

    [SerializeField] AudioClip _stop; //ne pas oublier d'ajouter component audiosource dans le gameobject
    AudioSource _audioPlayer;
    private float time = 0f;

    private void Awake()
    {
        _audioPlayer = GetComponent<AudioSource>();
    }


    // Update is called once per frame
    void Update()
    {
        if (time == 0)
        {
            _audioPlayer.clip = _stop;
            _audioPlayer.Play();
        }
        time += Time.deltaTime;
        if (time >= 30)
        {
            time = 0;
        }
    }

    public void StopSound()
    {
        _audioPlayer.Stop();
    }
}
