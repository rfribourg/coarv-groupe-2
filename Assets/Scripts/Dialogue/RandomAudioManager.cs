using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomAudioManager : MonoBehaviour
{

    private List<AudioSource> audioClipsActuel;
    [SerializeField] private List<AudioSource> audioClipsHub;
    [SerializeField] private List<AudioSource> audioClipsEnigme1;
    [SerializeField] private List<AudioSource> audioClipsSalleFinale;
    [SerializeField] private float tempsMinRandom = 20f;
    [SerializeField] private float tempsMaxRandom = 60f;

    private float nextPlayTime;
    private float time;
    private int playerState;

    void Start()
    {
        time = 0;
        nextPlayTime = Random.Range(tempsMinRandom, tempsMaxRandom);
        Debug.Log(nextPlayTime);
        playerState = 0;
    }

    void Update()
    {
        CheckState();

        time += Time.deltaTime;
        if (time >= nextPlayTime)
        {
            int randomIndex = Random.Range(0, audioClipsActuel.Count - 1);
            audioClipsActuel[randomIndex].Play();
            nextPlayTime = Random.Range(tempsMinRandom, tempsMaxRandom);
            Debug.Log(nextPlayTime);
            time = 0;
        }
    }

    void CheckState()
    {
        switch (playerState)
        {
            case 0: // Hub
                audioClipsActuel = audioClipsHub;
                break;
            case 1: // Enigme1
                audioClipsActuel = audioClipsEnigme1;
                break;
            case 2: // Salle Finale
                audioClipsActuel = audioClipsSalleFinale;
                break;
        }
    }

    public void SetPlayerState(int i)
    {
        playerState = i;
        time = 0;
    }

    public int GetPlayerState()
    {
        return playerState;
    }
}
