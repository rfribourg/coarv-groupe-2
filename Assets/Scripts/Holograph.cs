using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Holograph : MonoBehaviour
{
    public float RotationSpeed = 1;
    public float OscillationSpeed = 1;
    public float Amplitude = 1;
    private float temps = 0.0f;

    void Update()
    {
        temps += Time.deltaTime;
        gameObject.transform.Translate(Vector3.up * Mathf.Sin(temps * OscillationSpeed) * Amplitude, Space.World);
        gameObject.transform.Rotate(Vector3.up, Time.deltaTime * RotationSpeed);        
    }
}
