using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enigme1FirstPart : MonoBehaviour
{
    public List<GameObject> DigitalNumberScreens;
    public GameObject DarkLight;
    public List<GameObject> BrightLights;
    public List<GameObject> SnapZones;
    public GameObject BigDoor;
    public bool test;

    public GameObject HallwayObstacle2;

    //TEMP
    public bool PremiereEtapeFinie = false;
    public bool DeuxiemeEtapeFinie = false;
    private bool BluePlaced;
    private bool GreenPlaced;
    private bool RedPlaced;
    private bool YellowPlaced;

    private void Start()
    {
        //Setup les lumi�res comme il faut
        DarkLight.SetActive(true);
        foreach (GameObject light in BrightLights)
        {
            light.SetActive(false);
        }
    }

    private void Update()
    {
        //Recup�re les bool des snapZones
        BluePlaced = SnapZones[0].GetComponent<SnapZone>().BonTableau;
        GreenPlaced = SnapZones[1].GetComponent<SnapZone>().BonTableau;
        RedPlaced = SnapZones[2].GetComponent<SnapZone>().BonTableau;
        YellowPlaced = SnapZones[3].GetComponent<SnapZone>().BonTableau;

        if (PremiereEtapeFinie && GreenPlaced && RedPlaced && BluePlaced && YellowPlaced)
        {
            //open the door
            BigDoor.GetComponent<BigDoor>().OpenDoor();
            HallwayObstacle2.SetActive(false);
        }

        if (!PremiereEtapeFinie && GreenPlaced && RedPlaced && BluePlaced && YellowPlaced)
        {
            // Pour chaque ecran ajout� � la liste,
            // on recupere son script et on appelle la fonction pour allumer l'ecran
            foreach (GameObject screen in DigitalNumberScreens)
            {
                screen.GetComponent<DigitalScreenManager>().LightTheScreen();
            }

            // Turn on the roomlight
            DarkLight.SetActive(false);
            foreach (GameObject light in BrightLights)
            {
                light.SetActive(true);
            }
            PremiereEtapeFinie = true;
        }      
    }
}
