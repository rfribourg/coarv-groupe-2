using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.ProBuilder.Shapes;

public class SmallDoor : MonoBehaviour
{
    private float temps;
    [SerializeField] GameObject Door;
    [SerializeField] bool AutomaticDoor;
    public float openningSpeed = 1;
    private bool DoorMoving;
    private bool DoorOpenned;
    [SerializeField] private int doorNumber;

    private GestionNav Nav;

    private void Start()
    {
        if(GameObject.Find("GestionNav") != null)
        {
            Nav = GameObject.Find("GestionNav").GetComponent<GestionNav>();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (AutomaticDoor && other.gameObject.layer == 11)
        {
            OpenDoor();
        }
    }

    public void OpenDoor()
    {
        GetComponent<AudioSource>().Play();
        if (!DoorOpenned && !DoorMoving)
        {
            temps = 0.0f;
            DoorMoving = true;
            StartCoroutine(OpenDoorEnum());
            switch (doorNumber)
            {
                case 1: // Vers Enigme1
                    if (Nav != null)
                    {
                        Nav.Notify(2);
                    }
                    //Nav.Notify(2);
                    break;
                case 2: // Vers salle fin
                    if (Nav != null)
                    {
                        Nav.Notify(4);
                    }
                    //Nav.Notify(4);
                    break;
            }
        }
    }

    IEnumerator OpenDoorEnum()
    {
        while (temps < 2)
        {
            temps += Time.deltaTime;
            // Move the door
            Door.transform.Translate(-Vector3.forward * Time.deltaTime * openningSpeed, Space.Self);
            yield return new WaitForEndOfFrame();
        }
        DoorOpenned = true;
        DoorMoving = false;
    }

    public void CloseDoor()
    {
        if (DoorOpenned && !DoorMoving)
        {
            temps = 0.0f;
            DoorMoving = true;
            StartCoroutine(CloseDoorEnum());
        }
    }

    IEnumerator CloseDoorEnum()
    {
        while (temps < 2)
        {
            temps += Time.deltaTime;
            // Move the door
            Door.transform.Translate(Vector3.forward * Time.deltaTime * openningSpeed, Space.Self);
            yield return new WaitForEndOfFrame();
        }
        DoorOpenned = false;
        DoorMoving = false;
    }
}
