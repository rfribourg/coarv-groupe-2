using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DigitalNumberScreen : MonoBehaviour
{
    // Material to light up the pieces of the number
    [SerializeField] Material TurnedOnMat;

    // Il faut ajouter � cette liste de GameObject tous les
    // morceaux que l'on veut allumer pour former le nombre.
    public List<GameObject> PartsToLightUp;

    public void DigitalLightning()
    {
        foreach(GameObject part in PartsToLightUp)
        {
            part.GetComponent<Renderer>().material = TurnedOnMat;
        }
    }
}
