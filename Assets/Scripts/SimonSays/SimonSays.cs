using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimonSays : MonoBehaviour
{
    public GameObject HandLight;
    public Transform SpawnPoint;
    public AudioSource Victory;

    public Light CenterLight;
    public Light GoodLight;
    public Light WrongLight;

    public List<GameObject> Couleurs;

    private bool isPlaying = false;
    public bool gameEnded = false;
    private bool showingCombinaison = false;
    private bool simonOff = false;

    private List<int> PlayerCombinaison = new List<int>();
    private List<int> simonCombinaison = new List<int>();
    public int nombreRoundMax;
    private int tailleCombinaison;
    private int couleur; // (0,1,2,3) (bleu, jaune, rouge, vert)
    public float tempsMax;
    private float tempsEcoulee;
    private bool onCoroutine = false;
    public float VitesseClignotement = 3;

    #region InteractionAveclesBoutons
    public void BluePressed()
    {
        //On ajoute le chiffre � la combinaison et on joue l'audio
        PlayerCombinaison.Add(0);
        TurnOn(Couleurs[0]);
        //On remet le temps � 0 pour signifier que le joueur a appuy�
        tempsEcoulee = 0.0f;
    }

    public void YellowPressed()
    {
        PlayerCombinaison.Add(1);
        TurnOn(Couleurs[1]);
        tempsEcoulee = 0.0f;
    }

    public void RedPressed()
    {
        PlayerCombinaison.Add(2);
        TurnOn(Couleurs[2]);
        tempsEcoulee = 0.0f;
    }

    public void GreenPressed()
    {
        PlayerCombinaison.Add(3);
        TurnOn(Couleurs[3]);
        tempsEcoulee = 0.0f;
    }
    #endregion

    #region PublicMethods
    public void TurnOn(GameObject button)
    {
        //plays audio and light button
        button.GetComponentInChildren<Light>().intensity = 20;
        button.GetComponentInChildren<AudioSource>().Play();
    }

    public void TurnOff(GameObject button)
    {
        button.GetComponentInChildren<Light>().intensity = 0;
    }

    //Fonction d'initialisation, lancement du jeu
    public void StartPlaying()
    {
        if (!gameEnded && !isPlaying)
        {
            isPlaying = true;
            showingCombinaison = true;
            tailleCombinaison = 1;
            //Turn off the center light
            CenterLight.intensity = 0;
        }
        else
        {
            Debug.Log("You are already playing with simon or finished the game");
        }
    }
    #endregion

    #region PrivateMethods
    //Retourne vraie si le player a jou� la bonne combinaison
    private bool CheckCombinaison()
    {
        for (int i = 0; i < tailleCombinaison; i++)
        {
            if (PlayerCombinaison[i] != simonCombinaison[i])
            {
                return false;
            }
        }
        return true;
    }

    private void PLayerGotWrong()
    {
        //TODO - rajouter audio + signal lumineux
        StartCoroutine(WrongCombinaison());
        StopPlaying();
    }

    private void StopPlaying()
    {
        isPlaying = false;
    }

    private void CentreClignote()
    {
        CenterLight.intensity = (Mathf.Sin(Time.time * VitesseClignotement) + 1) * 10; //oscille entre 0 et 20 selon sinusoide
    }

    //Cette fonction est appel�e lorsque le player � r�ussi le jeu
    private void EndGame()
    {
        //Turn on all the lights
        for (int i = 0; i < 4; i++)
        {
            Couleurs[i].GetComponentInChildren<Light>().intensity = 20;
        }
        GoodLight.intensity = 20;
        gameEnded = true;
        isPlaying = false;
        simonOff = true;

        StartCoroutine(EndGameEnum());
    }
    #endregion

    #region IEnumerators
    IEnumerator ShowingColors()
    {
        onCoroutine = true;
        for (int i = 0; i < tailleCombinaison; i++)
        {
            TurnOn(Couleurs[simonCombinaison[i]]);
            yield return new WaitForSeconds(0.8f);
            TurnOff(Couleurs[simonCombinaison[i]]);
            yield return new WaitForSeconds(0.1f);

        }
        PlayerCombinaison.Clear();
        onCoroutine = false;
        showingCombinaison = false;
    }

    IEnumerator GoodCombinaison()
    {
        onCoroutine = true;
        yield return new WaitForSeconds(0.5f);
        GoodLight.intensity = 15;
        yield return new WaitForSeconds(0.8f);
        GoodLight.intensity = 0;
        yield return new WaitForSeconds(0.8f);
        onCoroutine = false;
    }

    IEnumerator WrongCombinaison()
    {
        onCoroutine = true;
        WrongLight.intensity = 15;
        yield return new WaitForSeconds(0.8f);
        WrongLight.intensity = 0;
        yield return new WaitForSeconds(0.8f);
        onCoroutine = false;
    }

    IEnumerator EndGameEnum()
    {
        Victory.Play();
        yield return new WaitForSeconds(7f);
        var handLight = Instantiate(HandLight, SpawnPoint.transform.position, SpawnPoint.rotation);
    }

    #endregion

    private void Update()
    {
        if (isPlaying && !gameEnded)
        {

            //Simon va jouer la nouvelle combinaison
            if (!onCoroutine && showingCombinaison)
            {
                tempsEcoulee = 0.0f;

                simonCombinaison.Clear();
                for (int i = 0; i < tailleCombinaison; i++)
                {
                    couleur = Random.Range(0, 4); //returns random int between 0 and 3
                    simonCombinaison.Add(couleur);
                }
                showingCombinaison = false;
                StartCoroutine(ShowingColors());
            }

            //C'est a nous de jouer
            if (!onCoroutine && !showingCombinaison)
            {
                tempsEcoulee += Time.deltaTime;
                //Si le joueur n'a pas appuy� depuis trop longtemps, la partie est perdue
                if (tempsEcoulee > tempsMax)
                {
                    StopPlaying();
                }

                //La liste PlayerCombinaison est modifiee a chaque fois que le joueur appuye sur un bouton
                if (PlayerCombinaison.Count == tailleCombinaison)
                {
                    //La combinaison est bonne
                    if (CheckCombinaison())
                    {
                        //Si le jeu n'est pas encore fini, on passe au round suivant
                        if (tailleCombinaison < nombreRoundMax)
                        {
                            StartCoroutine(GoodCombinaison());
                            tailleCombinaison += 1;
                            showingCombinaison = true;
                        }
                        else
                        {
                            gameEnded = true;
                        }
                    }
                    //Le combinaison n'est pas correcte, simon arrete de jouer
                    else
                    {
                        PLayerGotWrong();
                    }
                }
            }
        }
        else
        {
            CentreClignote();
        }

        if (gameEnded && !onCoroutine && !simonOff)
        {
            EndGame();
        }
    }
}
