using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakingEffect : MonoBehaviour
{
    public float lambda;
    public float frequenceOscillation;
    public float amplitude;

    private float temps;
    private Vector3 startingPos;

    void Start()
    {
        temps = 0;
        startingPos.x = transform.position.x;
        startingPos.y = transform.position.y;
        startingPos.z = transform.position.z;
    }


    void Update() {
        temps += Time.deltaTime;

        transform.localPosition = new Vector3(startingPos.x + SinusoideDecroissante(temps),
                                              startingPos.y,
                                              startingPos.z);
    }

    private float SinusoideDecroissante(float x)
    {
        return Mathf.Exp(- x * lambda) * Mathf.Sin(frequenceOscillation * x) * amplitude;
    }
}
