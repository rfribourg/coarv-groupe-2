using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Device;

public class DigitalScreenManager : MonoBehaviour
{
    public GameObject Ten;
    public GameObject Unit;

    public void LightTheScreen()
    {
        Ten.GetComponent<DigitalNumberScreen>().DigitalLightning();
        Unit.GetComponent<DigitalNumberScreen>().DigitalLightning();
    }
}
