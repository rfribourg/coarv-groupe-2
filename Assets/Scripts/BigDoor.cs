using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.ProBuilder.Shapes;

public class BigDoor : MonoBehaviour
{
    [SerializeField] GameObject leftDoor;
    [SerializeField] GameObject rightDoor;
    public float opennigSpeed = 1;
    private bool DoorOpenning;
    private bool DoorOpenned = false;
    private float temps;

    private GestionNav Nav;
    private void Start()
    {
        if (GameObject.Find("GestionNav") != null)
        {
            Nav = GameObject.Find("GestionNav").GetComponent<GestionNav>();
        }
    }

    public void OpenDoor()
    {
        GetComponent<AudioSource>().Play();
        if (!DoorOpenned && !DoorOpenning)
        {
            temps = 0.0f;
            DoorOpenning = true;
            StartCoroutine(OpenDoorEnum());
            Nav.Notify(3);
        }
    }

    IEnumerator OpenDoorEnum()
    {
        while (temps < 2)
        {
            temps += Time.deltaTime;
            // Move the doors
            rightDoor.transform.Translate(-Vector3.forward * Time.deltaTime * opennigSpeed, Space.Self);
            leftDoor.transform.Translate(Vector3.forward * Time.deltaTime * opennigSpeed, Space.Self);
            yield return new WaitForEndOfFrame();
        }
        DoorOpenned = true;
        DoorOpenning = false;
    }
}
