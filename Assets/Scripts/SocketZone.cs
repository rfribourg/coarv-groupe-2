using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SocketZone : MonoBehaviour
{
    public Transform ZoneTransform;

    //The gamreObject colliding with the snapzone
    private GameObject Object;

    //True if the object colliding with the zone is grabbed
    private bool grabbed = false;

    //True if an Object collides with the zone
    private bool insideSnapZone = false;

    //True if an object is snapped in the snapZone
    private bool snapped = false;

    private void OnTriggerEnter(Collider other)
    {
        if (!snapped) {
            Debug.LogWarning("inside Zone");
            Object = other.gameObject;
            insideSnapZone = true;
        }
        else
        {
            Debug.Log("Socket full!");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        other.GetComponent<Rigidbody>().isKinematic = true;
        insideSnapZone = false;
        Object = null;
    }

    private void SnapObject()
    {
        //Si on lache l'objet alors qu'il est dans la zone, il se snap
        if (!grabbed && insideSnapZone)
        {
            Object.transform.SetPositionAndRotation(ZoneTransform.position, ZoneTransform.rotation);
            Object.GetComponent<Rigidbody>().isKinematic = true;
            snapped = true;
        }
        
        if(!insideSnapZone)
        {
            snapped = false;
        }
    }

    private void Update()
    {
        if (insideSnapZone)
        {
            grabbed = Object.GetComponent<OVRGrabbable>().isGrabbed;
            SnapObject();
        }
        else
        {
            Debug.Log("no object in snap zone");
        }
    }


}
